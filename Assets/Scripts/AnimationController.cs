﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Animator))]
public class AnimationController : MonoBehaviour
{
    [SerializeField] bool starShine;
    
    private Animator animator;

    private void Awake()
    {
        animator = GetComponent<Animator>();

        if (starShine)
            StartCoroutine(CoPlay());
    }

    IEnumerator CoPlay()
    {
        float rand = UnityEngine.Random.Range(0f, 1f);
        yield return new WaitForSeconds(rand);
        Play("Star");
    }

    public void SetBoolOn(string name)
    {
        animator.SetBool(name, true);
    }

    public void SetBoolOff(string name)
    {
        animator.SetBool(name, false);
    }

    public void ToggleBool(string name)
    {
        animator.SetBool(name, !animator.GetBool(name));
    }

    private void OnPlayerDieListener(EventMessage obj)
    {
        animator.SetBool("Dead", true);
    }

    public void ResetAnimator()
    {
        animator.Rebind();
    }

    public void SetDirection(float value)
    {
        value = Mathf.Clamp(value, -1f, 1f);

        animator.SetFloat("Direction", value);
    }

    public void Play(string clip)
    {
        animator.Play(clip);
    }
}