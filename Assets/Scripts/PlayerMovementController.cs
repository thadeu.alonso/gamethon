﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.Events;

public class PlayerMovementController : MonoBehaviour
{
    [System.Serializable]
    public class Events
    {
        public UnityEventFloat OnHeightUpdate;
        public UnityEventFloat OnMoveSpeed;
        public UnityEventFloat OnDirectionUpdate;
        public UnityEvent OnStartFlying;
        public UnityEvent OnStopFlying;
        public UnityEventFloat OnPowerUpDurationChanged;
    }

    [Header("Movement Settings")]
    [SerializeField] float startMoveSpeed = 10; float moveSpeed = 5f; 
    [SerializeField] float minMoveSpeed;
    [SerializeField] float maxMoveSpeed;
    [SerializeField] float moveSidesSpeed = 5f;
    [SerializeField] float speedUpVariation = 1f;
    [SerializeField] float speedDownVariation;

    [Header("Fly Move Settings")]
    [SerializeField] float flyModeTime;
    [SerializeField] float flyMoveSpeed;
    [SerializeField] float moveUpSpeed;

    [SerializeField] LifeController lc;

    [SerializeField] Events events;

    private float originalMoveSpeed;
    private float originalGravityScale;
    private float currentHeight;
    private float verticalDirection;

    private Rigidbody2D rb2D;

    public bool IsFlying { get; set; }
    public bool IsGoingDown { get; set; }
    public bool IsDead { get; set; }

    float y;

    private void Awake()
    {
        Time.timeScale = 1;

        y = transform.position.y;
        ResetMoveUpSpeed();
        rb2D = GetComponent<Rigidbody2D>();
        moveSpeed = startMoveSpeed;

        if (GameManager.Instance.GameStarted)
            Initialize();
    }

    public void Initialize()
    {
        rb2D.isKinematic = false;
    }

    private void Update()
    {
        if (!GameManager.Instance.GameStarted || IsDead) return;

        UpdateHeight();

        // Só to usando essa forma porque o uso dos eventos para Slider não
        // tava dando certo e eu tava perdendo muito tempo. 
        EventManager.Instance.TriggerEvent(new MoveSpeedChanged(moveSpeed, maxMoveSpeed, flyMoveSpeed));
        events.OnMoveSpeed.Invoke(moveSpeed);

        if (transform.position.y > y)
        {
            UpdateHeights();

            rb2D.AddForce(Vector2.down *100);

            transform.position = new Vector3(transform.position.x, y, transform.position.z);
        }
    }

    private void FixedUpdate()
    {
        if (!GameManager.Instance.GameStarted) return;

        if (!IsGoingDown)
            MoveSide();

        if (IsFlying)
        {
            MoveUp();
            SpeedDown(0.4f);
        }
        else
        {
            SpeedDown(0.1f);
        }

        verticalDirection = rb2D.velocity.normalized.y;

        if(events.OnDirectionUpdate != null)
            events.OnDirectionUpdate.Invoke(verticalDirection);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "GameOver" && !lc.isDead)
        {
            ResetProgress();
            lc.Die();
        }
    }

    public void StartFlying()
    {
        IsFlying = true;
        rb2D.velocity = Vector2.zero;
        originalGravityScale = rb2D.gravityScale;
        rb2D.gravityScale = 0f;
        originalMoveSpeed = moveSpeed;
        moveSpeed = maxMoveSpeed;

        events.OnStartFlying.Invoke();
    }

    public void StopFlying()
    {
        IsFlying = false;
        rb2D.gravityScale = originalGravityScale;
        moveSpeed = startMoveSpeed;

        events.OnStopFlying.Invoke();
    }

    public void SpeedUp()
    {
        moveSpeed += speedUpVariation;
        moveSpeed = Mathf.Clamp(moveSpeed, minMoveSpeed, maxMoveSpeed);

        if (moveSpeed >= flyMoveSpeed)
        {
            StartCoroutine(CoFlying());
        }
    }

    public void SpeedDown(float multiplier)
    {
        if(!IsFlying)
            moveSpeed -= speedDownVariation * multiplier * Time.deltaTime;

        moveSpeed = Mathf.Clamp(moveSpeed, minMoveSpeed, maxMoveSpeed);
    }

    private void MoveSide()
    {
        float hor = Input.GetAxisRaw("Horizontal");
        rb2D.velocity = new Vector2(hor * moveSidesSpeed, rb2D.velocity.y);
    }

    private void MoveUp()
    {
        rb2D.velocity = new Vector2(rb2D.velocity.x, moveUpSpeed * 5);
    }

    private void UpdateHeight()
    {
        currentHeight += Mathf.Max(10, moveSpeed) * Time.deltaTime;
        events.OnHeightUpdate.Invoke(currentHeight);
    }
    private void UpdateHeights()
    {
        currentHeight += Mathf.Max(10, moveSpeed) * 2 * Time.deltaTime;
        events.OnHeightUpdate.Invoke(currentHeight);
    }

    public void ResetMoveUpSpeed()
    {
        moveSpeed = startMoveSpeed;
    }

    public void StopVelocity()
    {
        rb2D.velocity = Vector2.zero;
    }

    public void SetKinematic()
    {
        rb2D.isKinematic = true;
    }

    public void AddForceUp(float _force)
    {
        rb2D.AddForce(Vector2.up * _force, ForceMode2D.Impulse);
    }

    public void ResetProgress()
    {
        Debug.Log("Reset Progress");
        currentHeight = 0;
    }

    IEnumerator CoFlying()
    {
        StartFlying();

        float flyModeTimeCO = flyModeTime;
        events.OnPowerUpDurationChanged.Invoke(flyModeTimeCO * 60 / flyModeTime);

        while (flyModeTimeCO > 0)
        {
            yield return null;
            flyModeTimeCO -= Time.deltaTime;
            events.OnPowerUpDurationChanged.Invoke(flyModeTimeCO * 60 / flyModeTime);
        }

        StopFlying();
    }
}