﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class IntEventListener : MonoBehaviour
{
    [Serializable]
    public class Listener
    {
        public string name;
        public IntEvent Event;
        public UnityEventInt Response;
    }

    public List<Listener> listeners;

    private void OnEnable()
    {
        foreach (var listener in listeners)
            if (listener.Event != null)
                listener.Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        foreach (var listener in listeners)
            if (listener.Event != null)
                listener.Event.UnregisterListener(this);
    }

    public void OnEventRaised(IntEvent _event, int _value)
    {
        Listener listener = listeners.Find(x => x.Event == _event);

        if (listener != null)
            listener.Response.Invoke(_value);
    }
}
[System.Serializable]
public class UnityEventInt : UnityEvent<int>, ICustomEvent { }