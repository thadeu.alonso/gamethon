﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "NewIntEvent", menuName = "Events/Int Event")]
public class IntEvent : ScriptableObject
{
    private List<IntEventListener> listeners = new List<IntEventListener>();

    public void Raise(int _value)
    {
        for (int i = listeners.Count - 1; i >= 0; i--)
            listeners[i].OnEventRaised(this, _value);
    }

    public void RegisterListener(IntEventListener listener)
    {
        listeners.Add(listener);
    }

    public void UnregisterListener(IntEventListener listener)
    {
        listeners.Remove(listener);
    }
}