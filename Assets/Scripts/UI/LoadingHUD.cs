﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum Escene { MenuInicial, Loading, InGame0 }

public class LoadingHUD : HUD<LoadingHUD>
{
    [SerializeField] private Image loadingIcon;
    [SerializeField] private Slider loadingBar;
    [SerializeField] private TextMeshProUGUI loadingText;

    private void OnEnable()
    {
        EventManager.Instance.StartListening<LoadMessage>(SetLoadingBarValue);
    }

    private void OnDisable()
    {
        EventManager.Instance.StopListening<LoadMessage>(SetLoadingBarValue);
    }

    public void SetLoadingBarValue(EventMessage _msg)
    {
        LoadMessage msg = (LoadMessage)_msg;
        loadingBar.value = msg.percent;
        loadingText.text = Mathf.Round(msg.percent * 100) + "%";

        if (msg.percent == 1)
            loadingIcon.GetComponent<Animator>().speed = 0;
    }
}
