﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuInicialHUD : HUD<MenuInicialHUD>
{
    private void Start()
    {
        SoundManager.Instance.PlayMusic(EMusic.bg_music);
    }

    public void BtnJogar()
    {
        SoundManager.Instance.StopMusic();
        LoadManager.Instance.LoadScene(Escene.InGame0);
    }

    public void BtnCreditos()
    {
        Disable();
        CreditosHUD.Instance.Enable();
    }

    public void BtnAjustes()
    {
        Disable();
        AjustesHUD.Instance.Enable();
    }
}
