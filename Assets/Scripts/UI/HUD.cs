﻿using UnityEngine;

[RequireComponent (typeof(CanvasGroup))]
public class HUD<T> : MonoBehaviour
{
    private static T instance;
    public static T Instance { get { return instance; }}

    protected CanvasGroup thisCanvasGroup;

    protected virtual void Awake()
    {
        instance = this.GetComponent<T>();
        GetReference();
    }

    private void GetReference()
    {
        thisCanvasGroup = GetComponent<CanvasGroup>();
    }

    public void Enable()
    {
        thisCanvasGroup.alpha = 1;
        thisCanvasGroup.interactable = true;
        thisCanvasGroup.blocksRaycasts = true;
    }

    public void Disable()
    {
        thisCanvasGroup.alpha = 0;
        thisCanvasGroup.interactable = false;
        thisCanvasGroup.blocksRaycasts = false;
    }

    public void Toogle()
    {
        GetReference();
        if (!thisCanvasGroup.interactable)
            Enable();
        else
            Disable();
    }
}
