﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AjustesHUD : HUD<AjustesHUD>
{
    [SerializeField] private Slider musicVolumeSlider;
    public Slider MusicVolumeSlider { set { musicVolumeSlider = value; } get { return musicVolumeSlider; } }
    [SerializeField] private Slider sfxVolumeSlider;
    public Slider SfxVolumeSlider { set { sfxVolumeSlider = value; } get { return sfxVolumeSlider; } }

    [SerializeField] Dropdown qualities;

    protected override void Awake()
    {
        base.Awake();
        musicVolumeSlider.onValueChanged.AddListener(delegate { OnMusicVolumeSliderChanged(); });
        sfxVolumeSlider.onValueChanged.AddListener(delegate { OnSfxVolumeSliderChanged(); });
    }

    private void Start()
    {
        MusicVolumeSlider.value = PersistenceManager.Instance.CurrentGameInfo.musicAudioVolume;
        SfxVolumeSlider.value = PersistenceManager.Instance.CurrentGameInfo.sfxAudioVolume;

        FillQualityDropdown();
        LoadQuality();
        ApplyQuality();
    }

    public void BtnVoltar()
    {
        Salvar();
        MenuInicialHUD.Instance.Enable();
        this.Disable();
    }
    public void Salvar()
    {
        PersistenceManager.Instance.CurrentGameInfo.musicAudioVolume = musicVolumeSlider.value;
        PersistenceManager.Instance.CurrentGameInfo.sfxAudioVolume = sfxVolumeSlider.value;
        PersistenceManager.Instance.CurrentGameInfo.graphicQuality = qualities.value;
        ApplyQuality();
        PersistenceManager.Instance.SaveGameInfo();
    }

    public void OnMusicVolumeSliderChanged()
    {
        SoundManager.Instance.MusicVolume = musicVolumeSlider.value;
    }
    public void OnSfxVolumeSliderChanged()
    {
        SoundManager.Instance.SfxVolume = sfxVolumeSlider.value;
    }

    public void FillQualityDropdown()
    {
        string[] nomes = QualitySettings.names;
        qualities.options.Clear();
        for (int y = 0; y < nomes.Length; y++)
            qualities.options.Add(new Dropdown.OptionData() { text = nomes[y] });
    }

    public void LoadQuality()
    {
        qualities.value = PersistenceManager.Instance.CurrentGameInfo.graphicQuality;
    }

    public void ApplyQuality()
    {
        QualitySettings.SetQualityLevel(qualities.value);
    }
}
