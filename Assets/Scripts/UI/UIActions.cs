﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIActions : MonoBehaviour
{
    public void PauseGame(bool flag)
    {
        Time.timeScale = flag ? 0f : 1f;
    }

    public void SetText(float value)
    {
        TextMeshProUGUI text = GetComponent<TextMeshProUGUI>();
        
        if(text != null)
        {
            text.text = Mathf.RoundToInt(value).ToString();
        }
    }

    public void ShowPanel(GameObject go)
    {
        UIManager.Instance.ToggleCanvasGroup(go.name, true);
    }

    public void ShowPanelAfterTime(string element)
    {
        StartCoroutine(CoShowPanel(element, 1.5f));
    }

    IEnumerator CoShowPanel(string element, float delay)
    {
        yield return new WaitForSeconds(delay);
        GameObject go = GameObject.Find(element);
        ShowPanel(go);
    }

    public void ClosePanel(GameObject go)
    {
        UIManager.Instance.ToggleCanvasGroup(go.name, false);
    }

    public void TooglePanel(GameObject go)
    {
        CanvasGroup canvasGroup = go.GetComponent<CanvasGroup>();

        UIManager.Instance.ToggleCanvasGroup(canvasGroup);
    }

    //TODO: Alterar método para reposição de objetos na cena
    // ao inves de recarregar a cena
    public void RestartScene()
    {
        FindObjectOfType<SceneController>().RestartScene();
    }

    public void LoadScene(string scene)
    {
        LevelManager.Instance.LoadScene(scene);
    }

    public void LoadScene(int sceneIndex)
    {
        LevelManager.Instance.LoadScene(sceneIndex);
    }

    public void LoadLastScene()
    {
        LevelManager.Instance.LoadLastScene();
    }

    public void ToggleImage(Image image)
    {
        image.enabled = !image.enabled;
    }

    public void StartFadeCanvasGroupAlphaDuringTime(float _time)
    {
        StartCoroutine(FadeImageAlphaDuringTime(_time));
    }

    IEnumerator FadeImageAlphaDuringTime(float _time)
    {
        CanvasGroup canvasGroup = GetComponent<CanvasGroup>();

        if (canvasGroup == null)
            yield break;

        while(canvasGroup.alpha > 0)
        {
            yield return null;
            canvasGroup.alpha -= Time.deltaTime * (1/ _time);
            if (canvasGroup.alpha <= 0)
            {
                canvasGroup.alpha = 0;
                yield break;
            }
        }
    }

    public void StartSetThisButtonInteractableAfterTime(float _time)
    {
        StartCoroutine(SetThisButtonInteractableAfterTime(_time));
    }

    IEnumerator SetThisButtonInteractableAfterTime(float _time)
    {
        yield return new WaitForSeconds(_time);
        Button b = GetComponent<Button>();
        if (b != null)
            b.interactable = true;
    }
}
