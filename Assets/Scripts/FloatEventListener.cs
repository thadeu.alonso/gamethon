﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FloatEventListener : MonoBehaviour
{
    [Serializable]
    public class Listener
    {
        public string name;
        public FloatEvent Event;
        public UnityEventFloat Response;
    }

    public List<Listener> listeners;

    private void OnEnable()
    {
        foreach (var listener in listeners)
            if (listener.Event != null)
                listener.Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        foreach (var listener in listeners)
            if (listener.Event != null)
                listener.Event.UnregisterListener(this);
    }

    public void OnEventRaised(FloatEvent _event, float _value)
    {
        Listener listener = listeners.Find(x => x.Event == _event);

        if (listener != null)
            listener.Response.Invoke(_value);
    }
}
[System.Serializable]
public class UnityEventFloat : UnityEvent<float>, ICustomEvent { }
public interface ICustomEvent { }