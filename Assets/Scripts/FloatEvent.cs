﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "NewFloatEvent", menuName = "Events/Float Event")]
public class FloatEvent : ScriptableObject
{
    private List<FloatEventListener> listeners = new List<FloatEventListener>();

    public void Raise(float _value)
    {
        for (int i = listeners.Count - 1; i >= 0; i--)
            listeners[i].OnEventRaised(this, _value);
    }

    public void RegisterListener(FloatEventListener listener)
    {
        listeners.Add(listener);
    }

    public void UnregisterListener(FloatEventListener listener)
    {
        listeners.Remove(listener);
    }
}