﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class TransformController : MonoBehaviour
{
    [SerializeField] Vector3 direction;
    [SerializeField] float speed = 1;

    private Rigidbody2D rb2D;

    private void Awake()
    {
        rb2D = GetComponent<Rigidbody2D>();
        if (rb2D == null)
            rb2D = gameObject.AddComponent<Rigidbody2D>();

        rb2D.isKinematic = true;
    }

    private void Update()
    {
        if (!GameManager.Instance.GameStarted) return;

        rb2D.velocity = direction * speed;
        //transform.position += direction * speed * Time.deltaTime;
    }

    public void SetSpeed(float value)
    {
        speed = Mathf.Max(30, value) * 0.1f;
    }
}