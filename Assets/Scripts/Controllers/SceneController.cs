﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    [SerializeField] float _delay = 2;

    public void RestartSceneAfterTime()
    {
        StartCoroutine(RestartSceneAfterTimeCO(_delay));
    }

    IEnumerator RestartSceneAfterTimeCO(float _time)
    {
        yield return new WaitForSeconds(_time);
        RestartScene();
    }

    public void RestartScene()
    {
        //GameManager.Instance.GameStarted = false;
        //GameManager.Instance.TutorialStarted = false;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}