﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Tile : MonoBehaviour
{
    [SerializeField] UnityEvent OnTriggerGameOver;
    [SerializeField] UnityEvent OnTriggerGarbageCollector;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "GarbageCollector")
        {
            if (OnTriggerGarbageCollector != null)
                OnTriggerGarbageCollector.Invoke();
        }

        if(collision.gameObject.tag == "GameOver")
        {
            Debug.LogWarning("Trigger com game over");

            if(OnTriggerGameOver != null)
                OnTriggerGameOver.Invoke();
        }
    }
}