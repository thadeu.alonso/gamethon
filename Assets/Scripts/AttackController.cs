﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AttackController : MonoBehaviour
{
    [System.Serializable]
    public class Events
    {
        public UnityEvent Collided;
        public UnityEvent DestroyedSomeObject;
    }

    [SerializeField] GameObject explosionPrefab;
    [SerializeField] Events events;

    bool destroyed;    

    public bool IsGoingDown { get; set; }
    public bool IsFlying { get; set; }
    public bool IsDead { get; set; }

    private void Update()
    {
        if (!GameManager.Instance.GameStarted || IsDead) return;

        if (IsFlying)
        {
            destroyed = false;

            DestroyObstacle();
            DestroyObstacleSide();

            if (destroyed)
                events.DestroyedSomeObject.Invoke();
        }
    }

    void DestroyObstacle()
    {
        RaycastHit2D[] cols = Physics2D.RaycastAll(transform.position + Vector3.left / 2, Vector3.up, 2);

        foreach (RaycastHit2D col in cols)
        {
            if (col.collider.transform.tag == "UpObj")
            {
                Instantiate(explosionPrefab, col.collider.transform.position, Quaternion.identity);
                Destroy(col.collider.transform.gameObject);
                destroyed = true;
            }
        }
        cols = Physics2D.RaycastAll(transform.position + Vector3.right / 2, Vector3.up, 2);

        foreach (RaycastHit2D col in cols)
        {
            if (col.collider.transform.tag == "UpObj")
            {
                Instantiate(explosionPrefab, col.collider.transform.position, Quaternion.identity);
                Destroy(col.collider.transform.gameObject);
                destroyed = true;
            }
        }
    }

    void DestroyObstacleSide()
    {
        RaycastHit2D[] cols = Physics2D.RaycastAll(transform.position, Vector3.left, 2);

        foreach (RaycastHit2D col in cols)
        {
            if (col.collider.transform.tag == "UpObj")
            {
                Instantiate(explosionPrefab, col.collider.transform.position, Quaternion.identity);
                Destroy(col.collider.transform.gameObject);
                destroyed = true;
            }
        }

        cols = Physics2D.RaycastAll(transform.position, Vector3.right, 2);

        foreach (RaycastHit2D col in cols)
        {
            if (col.collider.transform.tag == "UpObj")
            {
                Instantiate(explosionPrefab, col.collider.transform.position, Quaternion.identity);
                Destroy(col.collider.transform.gameObject);
                destroyed = true;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!IsGoingDown)
            return;

        if (collision.tag == "ThrowedObj" || collision.tag == "UpObj")
        {
            MovementController moveCtrl = collision.GetComponent<MovementController>();

            if(moveCtrl != null)
            {
                moveCtrl.ImpulseRandomSide();
            }
            else
            {
                Instantiate(explosionPrefab, collision.gameObject.transform.position, Quaternion.identity);
                Destroy(collision.gameObject);
            }

            events.Collided.Invoke();
            IsGoingDown = false;
        }
    }
}
