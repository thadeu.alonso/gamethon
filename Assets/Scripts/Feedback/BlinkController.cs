﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlinkController : MonoBehaviour
{
    [SerializeField] Renderer renderer;
    [SerializeField] float blinkTime = 1.5f;
    [SerializeField] float blinkChangeCd = 0.1f;

    public void StartBlinking()
    {
        StartCoroutine(Blink(blinkTime));
    }

    IEnumerator Blink(float _blinkTime)
    {
        if (renderer == null)
            yield break;

        float blinkTimeCO = blinkTime;
        while (blinkTimeCO > 0)
        {
            yield return new WaitForSeconds(blinkChangeCd);
            renderer.enabled = !renderer.enabled;
            blinkTimeCO -= blinkChangeCd;
        }

        renderer.enabled = true;
    }
}
