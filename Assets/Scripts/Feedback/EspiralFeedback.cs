﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EspiralFeedback : MonoBehaviour
{
    [SerializeField] GameObject espiral;

    public void EnableEspiralAfterTime()
    {
        Invoke("EnableEspiral", 0.05f);
    }

    public void EnableEspiral()
    {
        espiral.gameObject.SetActive(true);
    }
}
