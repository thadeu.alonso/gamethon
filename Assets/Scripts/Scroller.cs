﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scroller : MonoBehaviour
{
    private float scrollSpeed;
    private MeshRenderer meshRenderer;

    private void Awake()
    {
        meshRenderer = GetComponent<MeshRenderer>();   
    }

    private void FixedUpdate()
    {
        Vector2 offset = new Vector2(0f, scrollSpeed * 0.25f * Time.deltaTime);

        meshRenderer.material.mainTextureOffset += offset;
    }

    public void UpdateMoveSpeed(float value)
    {
        scrollSpeed = Mathf.Max(2f, value);
    }
}