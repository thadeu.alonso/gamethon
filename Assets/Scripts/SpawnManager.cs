﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    [SerializeField] float timer = 1.5f;
    [SerializeField] float randomValue = 0.5f;
    [SerializeField] GameObject throwedObj;
    [SerializeField] float minX = -5;
    [SerializeField] float maxX = 5;
    [SerializeField] float y = 0;

    float nextSpawnTime;

    public bool CanSpawn { get; set; }

    private void Awake()
    {
        CanSpawn = true;
        SetNextSpawnTime();
    }

    private void Update()
    {
        if (!GameManager.Instance.GameStarted) return;

        if (Time.time > nextSpawnTime && CanSpawn)
        {
            Spawn();
            SetNextSpawnTime();
        }
    }

    void SetNextSpawnTime()
    {
        nextSpawnTime = Time.time + timer;
    }
     
    void Spawn()
    {
        if (throwedObj == null)
            return;
        Instantiate(throwedObj, new Vector3(Random.Range(minX, maxX), y, 0), Quaternion.identity);
    }
}
