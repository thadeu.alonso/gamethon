﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ImpulseController : MonoBehaviour
{
    [System.Serializable]
    public class Events
    {
        public UnityEvent OnGoingDown;
        public UnityEvent OnImpulseUp;
    }

    [SerializeField] float impulseForceDown;
    [SerializeField] float impulseForceUp;
    [SerializeField] Events events;

    private Rigidbody2D target;

    public bool IsFlying { get; set; }

    private void Awake()
    {
        target = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        if (!GameManager.Instance.GameStarted) return;

        if (!IsFlying)
            if (Input.GetKeyDown(KeyCode.Space))
                ImpulseDown();
    }

    public void ImpulseDown()
    {
        target.velocity = Vector2.zero;
        target.AddForce(Vector2.down * impulseForceDown, ForceMode2D.Impulse);
        events.OnGoingDown.Invoke();
    }

    public void ImpulseUp()
    {
        target.velocity = Vector2.zero;
        target.AddForce(Vector2.up * impulseForceUp, ForceMode2D.Impulse);
        events.OnImpulseUp.Invoke();
    }
}