﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class VoidEventListener : MonoBehaviour
{
    [Serializable]
    public class Listener
    {
        public string name;
        public VoidEvent Event;
        public UnityEvent Response;
    }

    public List<Listener> listeners;

    private void OnEnable()
    {
        foreach (var listener in listeners)
            listener.Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        foreach (var listener in listeners)
            listener.Event.UnregisterListener(this);
    }

    public void OnEventRaised(VoidEvent _event)
    {
        Listener listener = listeners.Find(x => x.Event == _event);

        if (listener != null)
            listener.Response.Invoke();
    }
}
