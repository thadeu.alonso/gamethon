﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    [SerializeField] AudioClip[] audioClips;
    [SerializeField] float[] audioClipsVolume;
    public float CurrentSfxVolume { get; set; }
    public bool Muted { get; set; }

    float explosionCD = 0.1f; float nextPossibleExplosionTime;

    private void Awake()
    {
        CurrentSfxVolume = 1;
    }

    public void PlaySfxIndex(int _clipIndex)
    {
        if (Muted || _clipIndex >= audioClips.Length || _clipIndex >= audioClipsVolume.Length)
            return;

        if (_clipIndex == 2)
        {
            if (Time.time > nextPossibleExplosionTime)
            {
                nextPossibleExplosionTime = Time.time + explosionCD;
                AudioSource.PlayClipAtPoint(audioClips[_clipIndex], Camera.main.transform.position, audioClipsVolume[_clipIndex]);
            }
            else
                AudioSource.PlayClipAtPoint(audioClips[_clipIndex], Camera.main.transform.position, audioClipsVolume[_clipIndex] / 2);
            return;
        }


        AudioSource.PlayClipAtPoint(audioClips[_clipIndex], Camera.main.transform.position, audioClipsVolume[_clipIndex]);
    }
}
