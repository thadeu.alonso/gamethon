﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "NewVoidEvent", menuName = "Events/Void Event")]
public class VoidEvent : ScriptableObject
{
    private List<VoidEventListener> listeners = new List<VoidEventListener>();

    public void Raise()
    {
        for (int i = listeners.Count - 1; i >= 0; i--)
        {
            listeners[i].OnEventRaised(this);
        }
    }

    public void RegisterListener(VoidEventListener listener)
    {
        listeners.Add(listener);
    }

    public void UnregisterListener(VoidEventListener listener)
    {
        listeners.Remove(listener);
    }
}