﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TimerCounterEvent : MonoBehaviour
{
    [SerializeField] float cooldown = 0.2f; float timer;
    [SerializeField] bool auto;
    [SerializeField] UnityEvent OnTimer;

    bool counting;

    private void OnEnable()
    {
        counting = true;
        timer = cooldown;
    }

    private void Awake()
    {
        counting = true;
        timer = cooldown;
    }

    private void Update()
    {
        if (auto && counting)
            Count();
    }

    public void Count()
    {
        if (timer > 0)
            timer -= Time.deltaTime;
        else
        {
            timer = cooldown;
            OnTimer.Invoke();
        }
    }

    public void DisableTimer()
    {
        timer = cooldown;
        counting = false;
    }
}
