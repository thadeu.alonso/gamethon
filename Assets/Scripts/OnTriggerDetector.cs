﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnTriggerDetector : MonoBehaviour
{
    [SerializeField] LayerMask layerMask;
    [SerializeField] bool triggerEnter;
    [SerializeField] bool collisionEnter;
    [SerializeField] bool triggerStay;
    [SerializeField] bool triggerExit;
    [SerializeField] UnityEventTransform OnCollide;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (triggerEnter)
            TryInvokeEvent(other);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (collisionEnter)
            TryInvokeEvent(other.collider);
    }

    //private void OnTriggerEnter(Collider other)
    //{
    //    if(triggerEnter)
    //        TryInvokeEvent(other);
    //}
    //private void OnTriggerStay(Collider other)
    //{
    //    if (triggerStay)
    //        TryInvokeEvent(other);
    //}
    //private void OnTriggerExit(Collider other)
    //{
    //    if (triggerExit)
    //        TryInvokeEvent(other);
    //}

    void TryInvokeEvent(Collider2D other)
    {
        if (!LayerMaskExtensions.HasLayer(layerMask, other.gameObject.layer))
            return;

        OnCollide.Invoke(other.transform);
    }
}

[System.Serializable]
public class UnityEventTransform : UnityEvent<Transform>
{

}