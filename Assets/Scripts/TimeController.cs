﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeController : MonoBehaviour
{
    float currentTime;

    public void SetTimeScaleDuringTime(float _time)
    {
        if (_time > currentTime)
        {
            Time.timeScale = 0.01f;
            currentTime = _time;
        }

        //StopAllCoroutines();
        //StartCoroutine(ChangeAndReturnTimeScale(0.01f, _time));
    }

    private void Update()
    {
        if (currentTime > 0)
        {
            currentTime -= Time.fixedDeltaTime;
            if (currentTime <= 0)
                Time.timeScale = 1;
        }
    }


    IEnumerator ChangeAndReturnTimeScale(float _timeScale, float _duration)
    {
        Time.timeScale = _timeScale;
        yield return new WaitForSeconds(_duration * _timeScale);
        Time.timeScale = 1;
    }
}
