﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileSpawner : MonoBehaviour
{
    [SerializeField] float spawnDelay;
    [SerializeField] List<GameObject> tileList;

    private float spawnTimer;

    public bool CanSpawnNext { get; set; }

    private void Awake()
    {
        spawnTimer = spawnDelay;
        CanSpawnNext = true;
    }

    private void Update()
    {
        if (!GameManager.Instance.GameStarted) return;

        if (CanSpawnNext)
        {
            if (spawnTimer <= 0f)
            {
                Spawn();
                spawnTimer = spawnDelay;
                CanSpawnNext = false;
            }
            else
            {
                spawnTimer -= Time.deltaTime;
            }
        }
    }

    private void Spawn()
    {
        if (tileList.Count > 0)
        {
            int randIndex = UnityEngine.Random.Range(0, tileList.Count);
            GameObject p = Instantiate(tileList[randIndex], transform.position, Quaternion.identity);
            p.SetActive (true);
        }
        else
        {
            Debug.LogError("List de tiles zerada");
        }
    }
}