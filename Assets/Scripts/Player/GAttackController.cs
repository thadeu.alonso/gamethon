﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GAttackController : MonoBehaviour
{
    [SerializeField] int attackDamage = 1;

    public void Attack(Transform _t)
    {
        LifeController lc = _t.GetComponent<LifeController>();
        if (lc == null)
            return;

        lc.ReceiveDamage(attackDamage);
    }
}
