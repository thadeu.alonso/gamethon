﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LifeController : MonoBehaviour
{
    [System.Serializable]
    public class Events
    {
        public UnityEvent onReceiveDamage;
        public UnityEvent onDie;
        public UnityEvent onAnyReceiveDamage;
        public UnityEvent onEndInvulnerable;
    }

    [SerializeField] int maxLife; int currentLife;
    [SerializeField] float invulneravelCooldown = 2;
    [SerializeField] Events events;

    bool isInvulnerable;
    [HideInInspector] public bool isDead;

    private void Awake()
    {
        currentLife = maxLife;
    }

    public void ReceiveDamage(int _damage)
    {
        if (isDead || isInvulnerable)
            return;

        currentLife -= _damage;

        if (currentLife <= 0)
            Die();
        else
        {
            isInvulnerable = true;
            StartCoroutine(EndInvulnerableAfterTime(invulneravelCooldown));

            events.onReceiveDamage.Invoke();
        }

        events.onAnyReceiveDamage.Invoke();
    }

    IEnumerator EndInvulnerableAfterTime(float _time)
    {
        yield return new WaitForSeconds(_time);
        isInvulnerable = false;
    }

    public void Die()
    {
        isDead = true;
        events.onDie.Invoke();
    }
}
