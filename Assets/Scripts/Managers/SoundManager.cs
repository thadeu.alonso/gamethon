﻿using UnityEngine;

public enum EMusic { bg_music }
public enum ESFX { }

public class SoundManager : Singleton<SoundManager>
{
    [SerializeField] private AudioClip[] musics;
    [SerializeField] private AudioSource musicSource;
    [SerializeField] private AudioClip[] sfxs;
    [SerializeField] private AudioSource[] sfxSources;
    [SerializeField] private float lowPitchRange = 0.95f;
    [SerializeField] private float highPitchRange = 1.05f;
    [Range(0, 1)] [SerializeField] private float musicVolume = 1;
    [Range(0, 1)] [SerializeField] private float sfxVolume = 1;

    public float SfxVolume
    {
        get
        {
            return sfxVolume;
        }
        set
        {
            for (int i = 0; i < sfxSources.Length; i++)
                sfxSources[i].volume = sfxVolume;
            sfxVolume = value;
        }
    }
    public float MusicVolume
    {
        get
        {
            return musicVolume;
        }
        set
        {
            musicVolume = value;
            musicSource.volume = musicVolume;
        }
    }

    public void PlaySFX(ESFX _sfx)
    {
        if (sfxs.Length - 1 < (int)_sfx || sfxSources == null)
            return;

        for (int i = 0; i < sfxSources.Length; i++)
        {
            if (!sfxSources[i].isPlaying)
            {
                PlaySFX(_sfx, i);
                return;
            }
        }
        PlaySFX(_sfx, 0);
    }

    void PlaySFX(ESFX _sfx, int sourceIndex)
    {
        sfxSources[sourceIndex].pitch = Random.Range(lowPitchRange, highPitchRange);
        sfxSources[sourceIndex].clip = sfxs[(int)_sfx];
        sfxSources[sourceIndex].Play();
    }

    public void PlayMusic(EMusic _music)
    {
        if (musics.Length - 1 < (int)_music || musicSource == null)
            return;

        musicSource.clip = musics[(int)_music];
        musicSource.Play();
    }

    public void StopMusic()
    {
        musicSource.Stop();
    }
}
