using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : Singleton<UIManager>
{
    [SerializeField] PlayerInfo playerInfo; 

    private void OnEnable()
    {
        EventManager.Instance.StartListening<MoveSpeedChanged>(OnMoveSpeedChanged);
        SceneManager.sceneLoaded += SceneManager_sceneLoaded;
    }

    private void OnDisable()
    {
        EventManager.Instance.StopListening<MoveSpeedChanged>(OnMoveSpeedChanged);
        SceneManager.sceneLoaded -= SceneManager_sceneLoaded;
    }

    private void SceneManager_sceneLoaded(Scene arg0, LoadSceneMode arg1)
    {
        if(GameManager.Instance.GameStarted)
            ToggleCanvasGroup("pnlTitleScreen", false);

        if (GameManager.Instance.TutorialStarted)
            ToggleCanvasGroup("pnlTutorial", false);
    }

    public void UpdateGameOverInfo()
    {
        SetTextUIElement("txtFinalHeight", Mathf.RoundToInt(GameManager.Instance.height).ToString());
        SetTextUIElement("txtHighScore", Mathf.RoundToInt(playerInfo.BestHeight).ToString());
    }

    private void OnMoveSpeedChanged(EventMessage obj)
    {
        MoveSpeedChanged msg = (MoveSpeedChanged)obj;

        //UpdateStatusBar("sldMoveSpeed", Mathf.RoundToInt(msg.MoveSpeed), Mathf.RoundToInt(msg.FlyMoveSpeed));
    }

    public void UpdateStatusBar(string elementName, int currentValue, int maxValue)
    {
        GameObject bar = GameObject.Find(elementName);

        if (bar != null)
        {
            bar.GetComponent<Slider>().value = currentValue;
            bar.GetComponent<Slider>().maxValue = maxValue;

            if (bar.GetComponentInChildren<Text>() != null)
                bar.GetComponentInChildren<Text>().text = currentValue + " / " + maxValue;
        }
    }

    public void SetMaxValueBar(string elementName, int value)
    {
        Slider bar = GameObject.Find(elementName).GetComponent<Slider>();
        bar.maxValue = value;
    }

    public void ToggleCanvasGroup(CanvasGroup element)
    {
        CanvasGroup group = element;

        group.blocksRaycasts = !group.blocksRaycasts;
        group.alpha = group.blocksRaycasts ? 1 : 0;
    }

    public void ToggleCanvasGroup(CanvasGroup element, bool active)
    {
        CanvasGroup group = element;

        group.blocksRaycasts = active;
        group.alpha = group.blocksRaycasts ? 1 : 0;
    }

    public void ToggleCanvasGroup(string element)
    {
        CanvasGroup group = GameObject.Find(element).GetComponent<CanvasGroup>();

        group.blocksRaycasts = !group.blocksRaycasts;
        group.alpha = group.blocksRaycasts ? 1 : 0;
    }

    public void ToggleCanvasGroup(string element, bool active)
    {
        if(GameObject.Find(element) && GameObject.Find(element).GetComponent<CanvasGroup>())
        {
            CanvasGroup group = GameObject.Find(element).GetComponent<CanvasGroup>();

            group.blocksRaycasts = active;
            group.alpha = group.blocksRaycasts ? 1 : 0;
        }
    }

    public Toggle GetToogleElement(string toggleName)
    {
        return GameObject.Find(toggleName).GetComponent<Toggle>();
    }

    public Button GetButtonByName(string btnName)
    {
        if(GameObject.Find(btnName) != null)
            return GameObject.Find(btnName).GetComponent<Button>();
        return null;
    }

    public void SetTextUIElement(string elementName, string text)
    {
        Text normalText = GameObject.Find(elementName).GetComponentInChildren<Text>();
        TextMeshProUGUI meshProText;

        if (!normalText)
        {
            meshProText = GameObject.Find(elementName).GetComponentInChildren<TextMeshProUGUI>();
            meshProText.text = text;
        }
        else
        {
            normalText.text = text;
        }
    }

    public void ToggleImage(string imageName)
    {
        Image image = GameObject.Find(imageName).GetComponent<Image>();

        image.enabled = !image.enabled;
    }

    public void ToggleImage(string imageName, bool active)
    {
        Image image = GameObject.Find(imageName).GetComponent<Image>();

        image.enabled = active;
    }

    public void SetImageColor(string imageName, Color color)
    {
        Image image = GameObject.Find(imageName).GetComponent<Image>();

        image.color = color;
    }
}