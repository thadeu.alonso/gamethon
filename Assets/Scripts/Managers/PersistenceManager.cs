﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersistenceManager : Singleton<PersistenceManager>
{
    private string gameSavedInfoPath = "savedGame";
    public GameInfo CurrentGameInfo { set; get; }

    protected override void Awake()
    {
        base.Awake();
        LoadGameInfo();
    }
    
    public void SaveGameInfo()
    {
        PlayerPrefs.SetString("savedGame", JsonUtility.ToJson(CurrentGameInfo));
    }

    private void LoadGameInfo()
    {
        CurrentGameInfo = JsonUtility.FromJson<GameInfo>(PlayerPrefs.GetString("savedGame", JsonUtility.ToJson(new GameInfo())));
    }
}