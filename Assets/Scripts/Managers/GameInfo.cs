﻿[System.Serializable]
public class GameInfo
{
    public float musicAudioVolume = 1;
    public float sfxAudioVolume = 1;
    public int graphicQuality = 0;
}