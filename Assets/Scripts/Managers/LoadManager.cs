﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadManager : Singleton<LoadManager>
{
    private Escene sceneToLoad;

    public void LoadScene(Escene _scene)
    {
        sceneToLoad = _scene;
        StartCoroutine(COLoadSceneAsync(sceneToLoad));
    }

    IEnumerator COLoadSceneAsync(Escene _scene)
    {
        string oldSceneName = SceneManager.GetActiveScene().name;

        // Carrega a cena de loading
        AsyncOperation asyncLoader = SceneManager.LoadSceneAsync(Escene.Loading.ToString(), LoadSceneMode.Additive);
        while (!asyncLoader.isDone) { yield return null; }

        // Descarrega cena antiga
        GameObject[] oldSceneObjects = SceneManager.GetSceneByName(oldSceneName).GetRootGameObjects();
        for (int i = 0; i < oldSceneObjects.Length; i++)
        {
            Destroy(oldSceneObjects[i]);
            EventManager.Instance.TriggerEvent(new LoadMessage((i / oldSceneObjects.Length * 200)));
            yield return null;
        }
        asyncLoader = SceneManager.UnloadSceneAsync(SceneManager.GetSceneByName(oldSceneName));
        while (!asyncLoader.isDone) { yield return null; }
        EventManager.Instance.TriggerEvent(new LoadMessage(0.5f));
        yield return new WaitForSeconds(1);

        // Carrega nova cena
        asyncLoader = SceneManager.LoadSceneAsync(_scene.ToString(), LoadSceneMode.Additive);
        float progress = 0;
        while (!asyncLoader.isDone)
        {
            if (asyncLoader.progress > progress)
            {
                progress = asyncLoader.progress;
                EventManager.Instance.TriggerEvent(new LoadMessage(0.5f + progress / 2));
            }
            yield return null;
        }
        EventManager.Instance.TriggerEvent(new LoadMessage(1));
        yield return new WaitForSeconds(1);

        // Descarrega cena de loading
        SceneManager.UnloadSceneAsync(SceneManager.GetSceneByName(Escene.Loading.ToString()));
    }
}
