﻿using System.Collections.Generic;
using System;

public class EventManager : Singleton<EventManager>
{
    private Dictionary<Type, Action<EventMessage>> eventDictionary = new Dictionary<Type, Action<EventMessage>>();

    public void StartListening<MessageType>(Action<EventMessage> listener)
    {
        Action<EventMessage> thisEvent;

        if (Instance.eventDictionary.TryGetValue(typeof(MessageType), out thisEvent))
        {
            thisEvent += listener;
            Instance.eventDictionary[typeof(MessageType)] = thisEvent;
        }
        else
        {
            thisEvent += listener;
            Instance.eventDictionary.Add(typeof(MessageType), thisEvent);
        }
    }

    public void StopListening<MessageType>(Action<EventMessage> listener)
    {
        if (Instance == null) return;

        Action<EventMessage> thisEvent;

        if (Instance.eventDictionary.TryGetValue(typeof(MessageType), out thisEvent))
        {
            thisEvent -= listener;
            Instance.eventDictionary[typeof(MessageType)] = thisEvent;
        }
    }

    public void TriggerEvent(EventMessage message)
    {
        Action<EventMessage> thisEvent = null;

        if (Instance.eventDictionary.TryGetValue(message.GetType(), out thisEvent))
        {
            thisEvent.Invoke(message);
        }
    }
}

public abstract class EventMessage { }

public class MoveSpeedChanged : EventMessage
{
    public float MoveSpeed { get; set; }
    public float MaxMoveSpeed { get; set; }
    public float FlyMoveSpeed { get; set; }

    public MoveSpeedChanged(float moveSpeed, float maxMoveSpeed, float flyMoveSpeed)
    {
        MoveSpeed = moveSpeed;
        MaxMoveSpeed = maxMoveSpeed;
        FlyMoveSpeed = flyMoveSpeed;
    }
}

public class LoadMessage : EventMessage
{
    public float percent;

    public LoadMessage(float _percent)
    {
        percent = _percent;
    }
}