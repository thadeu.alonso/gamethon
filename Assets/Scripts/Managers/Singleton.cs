﻿using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T instance;
    public static T Instance
    {
        get
        {
            if (instance == null)
            {
                GameObject manager = new GameObject(typeof(T).Name);
                manager.AddComponent<T>();
                instance = manager.GetComponent<T>();
                DontDestroyOnLoad(manager);
            }

            return instance;
        }
    }

    protected virtual void Awake()
    {
        if (instance != null)
        {
            if (instance != this)
                Destroy(gameObject);
        }
        else
        {
            instance = this.GetComponent<T>();
            DontDestroyOnLoad(instance);
        }
    }
}