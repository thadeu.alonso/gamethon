﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : Singleton<GameManager>
{
    [SerializeField] PlayerInfo playerInfo;
    
    public bool TutorialStarted { get; set; }
    public bool GameStarted { get; set; }

    public float height;

    public void CalculateHighScore()
    {
        if (height > playerInfo.BestHeight)
            playerInfo.BestHeight = height;
    }

    public void SetHeight(float value)
    {
        height = value;
    }
}