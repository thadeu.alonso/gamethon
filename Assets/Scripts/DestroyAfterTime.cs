﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfterTime : MonoBehaviour
{
    [SerializeField] float timer = 1;
    [SerializeField] bool onEnable;

    private void Awake()
    {
        if (onEnable)
            StartTimer(timer);
    }


    public void StartTimer(float _time)
    {
        StartCoroutine(CODestroyAfterTime(_time));
    }

    IEnumerator CODestroyAfterTime(float _time)
    {
        yield return new WaitForSeconds(_time);

        Destroy(this.gameObject);
    }
}
