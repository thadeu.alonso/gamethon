﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HeightController : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI txtHeight;

    private int height;

    private void Awake()
    {
        height = 0;   
    }

    private void Update()
    {
        height = Mathf.RoundToInt(Time.time * 10f);
        txtHeight.text = height.ToString();
    }
}