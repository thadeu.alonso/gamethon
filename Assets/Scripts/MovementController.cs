﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MovementController : MonoBehaviour
{
    [System.Serializable]
    public class Events
    {
        public UnityEvent onExplode;
        public UnityEvent onAppearScreen;
    }

    [SerializeField] Rigidbody2D rb;
    [SerializeField] float initialForce = 10;
    [SerializeField] GameObject explosionPrefab;
    [SerializeField] GameObject starShinePrefab;

    [SerializeField] Events events;

    bool side;
    int sside;

    float y;

    private void Awake()
    {
        Invoke("OnAppearScreen", 0.5f);

        ImpulseUp();

        if (transform.position.x > 10)
        {
            side = true;
            sside = 1;
        }
        if (transform.position.x < -10)
        {
            side = true;
            sside = -1;
        }
        if (side)
        {
            y = Random.Range(-4, -2f);
            transform.position = new Vector3(transform.position.x, y, transform.position.z);
        }
    }

    private void Update()
    {
        if (side)
            transform.position = new Vector3(transform.position.x, y, transform.position.z);
    }

    public void ImpulseUp()
    {
        if (rb == null)
            return;
        
        rb.AddForce(new Vector3(-transform.position.x + Random.Range(-3, 3), initialForce, 0), ForceMode2D.Impulse);
    }
    public void ImpulseDown(float _force)
    {
        if (rb == null)
            return;

        rb.AddForce(new Vector3(0, _force, 0), ForceMode2D.Impulse);
    }

    public void ImpulseRandomSide()
    {
        float rand = Random.Range(-1f, 1f);
        rb.velocity = Vector2.zero;
        rb.AddForce(new Vector2(rand * initialForce, rand * initialForce), ForceMode2D.Impulse);
        StartCoroutine(CoDestroyWithExplosion(0.5f));
        
    }

    public void OnAppearScreen()
    {
        events.onAppearScreen.Invoke();
    }

    IEnumerator CoDestroyWithExplosion(float delay)
    {
        Collider2D col = GetComponent<Collider2D>();

        if (col)
            col.enabled = false;

        DestroyAfterTime destroyScript = GetComponent<DestroyAfterTime>();
        destroyScript.StartTimer(delay);
        GetComponent<Animator>().SetBool("Dead", true);
        Instantiate(starShinePrefab, transform.position, Quaternion.identity, transform);

        yield return new WaitForSeconds(delay);

        events.onExplode.Invoke();
        Instantiate(explosionPrefab, transform.position, Quaternion.identity);
    }
}
