﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(HUD<>), true)]
public class HUDEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if (GUILayout.Button("Toggle", GUILayout.Width(50), GUILayout.Height(25)))
            Selection.activeGameObject.SendMessage("Toogle", SendMessageOptions.RequireReceiver);
    }
}
